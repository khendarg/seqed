from . import types
import random
from Bio import AlignIO, Align, SeqIO, Seq
import re

class SeqedModel(object):
    def __init__(self, **kwargs):
        self.msas = types.ListDict()
        self.active_seqrecord = None
        self.active_msa = None

    def _operate_on_sequences(self, targetmsa=None, targetseq=None, hit=None, miss=None):
        """Apply callbacks to all matching sequences in an MSA

        Parameters
        ----------
        targetmsa : NoneType or int or MultipleSeqAlignment 
            Which MSA to operate on. The default value of None directs this
            method to act on self.active_msa.
        targetseq : NoneType or int or list or range or slice or str
            Which sequence(s) to operate on. Ints are directly interpreted as 
            sequence indices while slices, lists, and ranges are iterated over 
            for sequence indices. Strings are also accepted, and strings 
            enclosed by forward slashes are treated as regular expressions.
        hit : 1-argument callback or NoneType
            Method to perform on matching sequences in the targeted MSA. The 
            default value of None generates NOOP lambdas.
        miss : 1-argument callback or NoneType
            Method to perform on non-matching sequences in the targeted MSA. The
            default value of None generates NOOP lambdas.
        """
        hit = (lambda record: None) if hit is None else hit
        miss = (lambda record: None) if miss is None else miss
        
        targetmsa = self.active_msa if targetmsa is None else targetmsa

        hits, misses = list(), list()
        for seqindex, seqrecord in enumerate(targetmsa):
            if type(targetseq) is list:
                if seqindex in targetseq or seqrecord in targetseq:
                    hit(seqrecord)
                else:
                    miss(seqrecord)
            elif type(targetseq) is int:
                if seqindex == targetseq:
                    hit(seqrecord) 
                else:
                    miss(seqrecord)
            elif type(targetseq) is str:
                if targetseq.startswith("/") and targetseq.endswith("/"):
                    if re.search(targetseq[1:-1], seqrecord.id):
                        hit(seqrecord)
                    else:
                        miss(seqrecord)
                else:
                    if targetseq == seqrecord.id:
                        hit(seqrecord)
                    else:
                        miss(seqrecord)
            else:
                if targetseq is None:
                    hit(seqrecord)
                elif targetseq is seqrecord:
                    hit(seqrecord)
                else:
                    miss(seqrecord)

    def _parse_indices(self, index, length=None, descending=False):
        """Return generators for indices in various common formats

        Parameters
        ----------
        index : int or range or slice or iterable of ints
            An int argument here is treated literally and returns a generator
            yielding only itself. A range, slice, or iterable of ints is 
            iterated over for indices.
        length : int or NoneType
            Length of domain. Used for correctly interpreting negative indices.
        descending : bool
            Force monotonically descending output. Useful for destructive list
            operations.
        """
        if type(index) is int:
            yield index
        elif type(index) is range:
            if descending and index.step > 0:
                for _ in reversed(index): yield _
            else:
                for _ in index: yield _
        elif type(index) is slice:
            if length is not None:
                if index.start is None: start = 0
                elif index.start >= 0: start = index.start
                else: start = index.start % length

                if index.stop is None: stop = 0
                elif index.stop >= 0: stop = index.stop
                else: stop = index.stop % length

                if descending and index.step > 0:
                    for _ in reversed(range(start, stop, index.step)): yield _
                else:
                    for _ in range(start, stop, index.step): yield _
            else:
                if descending and index.step > 0:
                    for _ in reversed(range(index.start, index.stop, index.step)): yield _
                else:
                    for _ in range(index.start, index.stop, index.step): yield _
        else:
            if descending:
                for _ in reversed(sorted(index)): yield _
            else:
                for _ in iter(index): 
                    #XXX this could cause major problems in the future...
                    for __ in self._parse_indices(_, length=length, descending=descending): yield __

    def insert(self, subseq, position=0, targetmsa=None, targetseq=None):
        """Insert a sequence into an MSA

        Parameters
        ----------
        subseq : str
            Sequence to insert.
        position : int
            Position before which subseq should be inserted.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.
        """
        self._operate_on_sequences(targetmsa=targetmsa, targetseq=targetseq,
                hit=lambda record: [record.seq.insert(position, _) for _ in subseq[::-1]],
                miss=lambda record: [record.seq.insert(position, "-") for _ in range(len(subseq))],
                )

    def append(self, *args, **kwargs):
        """Alias for extend

        Parameters
        ----------
        subseq : str
            Sequence to append.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.
        """
        self.extend(*args, **kwargs)

    def extend(self, subseq, targetmsa=None, targetseq=None):
        """Append sequences to the end of an MSA

        Parameters
        ----------
        subseq : str
            Sequences to append.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.
        """
        self._operate_on_sequences(targetmsa=targetmsa, targetseq=targetseq,
                hit=lambda record: record.seq.extend(subseq),
                miss=lambda record: record.seq.extend("-"*len(subseq)),
                )

    def delete_column(self, index, targetmsa=None):
        """Remove a column from an MSA

        Parameters
        ----------
        index : int
            Which column to remove.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        """
        targetmsa = self.active_msa if targetmsa is None else targetmsa
        indices = list(self._parse_indices(index, targetmsa.get_alignment_length(), descending=True))
        self._operate_on_sequences(targetmsa=targetmsa,
                hit=lambda record: [record.seq.__delitem__(_) for _ in indices],
                miss=lambda record: [record.seq.__delitem__(_) for _ in indices],
                )

    def delete(self, index, targetmsa=None, targetseq=None):
        """Replace residues with gaps for specific columns

        Parameters
        ----------
        index : int or list of ints or range or slice
            Positions to fill with gaps. See _parse_indices for details.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.
        """
        targetmsa = self.active_msa if targetmsa is None else targetmsa
        indices = list(self._parse_indices(index, targetmsa.get_alignment_length()))
        self._operate_on_sequences(targetmsa=targetmsa, targetseq=targetseq,
                hit=lambda record: [record.seq.__setitem__(_, "-") for _ in indices],
                miss=lambda record: None,
                )

    def grep(self, pattern, targetmsa=None, targetseq=None):
        """Find matches in an MSA

        Note that this method as currently implemented does not return overlapping hits. This may be a concern for low-complexity sequences with extensive repeats.

        Parameters
        ----------
        pattern : str
            Pattern to search for. Accepts Python regular expressions.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.

        Returns
        -------
        allhits : dict
            A dictionary containing hits binned by sequence ID
        """

        targetmsa = self.active_msa if targetmsa is None else targetmsa
        hitseq = []
        self._operate_on_sequences(targetmsa=targetmsa, targetseq=targetseq,
                hit=lambda record: hitseq.append(record),
                miss=lambda record: None,
                )
        allhits = {}
        for record in hitseq:
            recordhits = list(re.finditer(pattern, str(record.seq)))
            if recordhits: 
                if record.id not in allhits:
                    allhits[record.id] = []
                allhits[record.id].extend(recordhits)
        return allhits

    def random(self, count, alphabet):
        pass

    def mutate(self, seqindex, alphabet="ACDEFGHIKLMNPQRSTVWY"):
        pass

    def mutate_point(self, seqindex):
        pass

    def mutate_markov(self):
        pass

    def print(self):
        pass

    def load(self, handle, format=None, name=None):
        """Load all MSAs from a handle into memory

        Parameters
        ----------
        handle : file-like or str or path
            Whence to load the MSAs.
        format : str or NoneType
            MSA format. Limited autodetection is implemented for common FASTA
            and CLUSTAL presentations, but this should be set explicitly
            whenever possible.
        name : NoneType or str
            Internal name for MSA. Defaults to generation via gen_unused_msaname
        """
        if "tell" in dir(handle):
            pos = handle.tell()
            header = handle.read(7)
            if header.startswith(">"): format = "fasta"
            elif header.startswith("CLUSTAL"): format = "clustal"
            else: raise NotImplementedError("Format autodetection not implemented for header starting with '{}'".format(header))
            handle.seek(pos)
        elif "suffix" in dir(handle):
            suffix = handle.suffix
            if "fa" in suffix.lower(): format = "fasta"
            elif "a2m" in suffix.lower(): format = "fasta"
            elif "a3m" in suffix.lower(): format = "fasta"
            elif "aln" in suffix.lower(): format = "clustal"
            elif "clu" in suffix.lower(): format = "clustal"
            else: raise NotImplementedError("Format autodetection not implemented for file extension '{}'".format(suffix))
        elif "__contains__" in dir(handle):
            if "fa" in handle.lower(): format = "fasta"
            elif "a2m" in handle.lower(): format = "fasta"
            elif "a3m" in handle.lower(): format = "fasta"
            elif "aln" in handle.lower(): format = "clustal"
            elif "clu" in handle.lower(): format = "clustal"
            else: raise NotImplementedError("Format autodetection not implemented for filename '{}'".format(handle))
        elif format is None:
            raise TypeError("Could not autodetect format for handle '{}'".format(handle))

        if type(name) is str: names = [name]
        elif name is None: names = []
        else: names = name

        msalist = list(AlignIO.parse(handle, format))
        for msaindex, msa in enumerate(msalist):
            if msaindex < len(names): name = names[msaindex]
            else: name = self.gen_unused_msaname()

            for seqrecord in msa:
                oldseq = seqrecord.seq
                seqrecord.seq = Seq.MutableSeq(str(seqrecord.seq))
                del oldseq

            self.msas[name] = msa
            self.active_msa = msa

    def unload(self, targetmsa=None):
        targetmsa = self.active_msa if targetmsa is None else targetmsa
        self.msas.remove(targetmsa)

    def save(self, handle, targetmsa=None, format="fasta"):
        """Save an MSA to a handle

        Parameters
        ----------
        handle : file-like or str or path
            Whither to save the MSAs.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        format : str or NoneType
            MSA format. All formats implemented in Biopython are available,
            but this defaults to "fasta."
        """
        if str(handle) == "stdout": handle = "/dev/stdout"
        if str(handle) == "-": handle = "/dev/stdout"
        if str(handle) == "stderr": handle = "/dev/stderr"

        if format is None:
            if "suffix" in dir(handle):
                if "fa" in handle.suffix: format = "fasta"
                elif "aln" in handle.suffix: format = "clustal"
                elif "clu" in handle.suffix: format = "clustal"
                else: format = "fasta"
            elif "endswith" in dir(handle):
                if "fa" in handle.lower(): format = "fasta"
                elif "aln" in handle.lower(): format = "clustal"
                elif "clu" in handle.lower(): format = "clustal"
                else: format = "fasta"
            else: format = "fasta"

        targetmsa = self.active_msa if targetmsa is None else targetmsa
        if type(targetmsa) in (int, str): targetmsa = [self.msas.get(targetmsa)]
        elif type(targetmsa) is list:
            targetmsa = [(self.msas.get(_) if type(_) in (int, str) else _) for _ in targetmsa]
        AlignIO.write(targetmsa, handle, format)

    def create_seq(self, targetmsa=None, name=None, seq=None):
        """Create a new sequence in an MSA

        Parameters
        ----------
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        name : NoneType or str
            Name of sequence. Defaults to calling gen_unused_seqname to generate
            a random sequence name.
        seq : NoneType or str
            Sequence to add if any. Defaults to empty.
        """
        msa = self.resolve(targetmsa)
        if msa is None:
            if self.msas: self.active_msa = self.active_msa
            else: self.active_msa = self.msas[self.gen_unused_msaname()] = Align.MultipleSeqAlignment([])
        else:
            self.active_msa = msa

        if seq is not None:
            if self.active_msa is None: pass
            elif len(self.active_msa):
                seq = seq[:self.active_msa.get_alignment_length()]
                seq += "-" * (self.active_msa.get_alignment_length() - len(seq))
            else: seq = ""
        else: 
            if self.active_msa is None: seq = ""
            elif self.active_msa: seq = "-" * self.active_msa.get_alignment_length()
            else: seq = ""
        
        name = self.gen_unused_seqname(self.active_msa) if name is None else name
        active_seq = Seq.MutableSeq(seq)
        self.active_seqrecord = SeqIO.SeqRecord(id=name, name="", description="", seq=active_seq)
        self.active_msa.append(self.active_seqrecord)

    def gen_unused_msaname(self):
        """Generate a new MSA name"""
        prefix = "untitled"
        suffix = "msa"
        key = "{}{}".format(prefix, suffix)
        if key not in self.msas: return key
        n = 0
        while 1:
            n += 1
            key = "{}{}_{:03d}".format(prefix, suffix, n)

            if key not in self.msas.keys(): return key

    def gen_unused_seqname(self, targetmsa=None):
        """Generate a new sequence name

        Parameters
        ----------
        targetmsa : NoneType or MSA
            MSA to check against for existing names
        """
        targetmsa = self.active_msa if targetmsa is None else targetmsa
        prefix = "untitled"
        suffix = ""

        n = 0
        while 1:
            key = "{}{}_{:012d}".format(prefix, suffix, n)
            if not any([key == record.id for record in targetmsa]): return key
            n += 1
        
    def dump(self):
        pass

    def resolve(self, query):
        return self.msas.get(query)

    def __len__(self):
        return len(self.msas)

    def get_shape(self):
        """Return a representation of the workspace's shape as a tuple 
        containing the number of MSAs, a list of MSA sequence counts, and a 
        list of MSA lengths.
        """
        return (len(self.msas), [len(self.msas[msa]) for msa in self.msas], [self.msas[msa].get_alignment_length() for msa in self.msas])

    def set_active(self, targetmsa=None):
        """Change the active MSA

        Parameters
        ----------
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        """
        targetmsa = self.active_msa if targetmsa is None else targetmsa
        if targetmsa is None and len(self.msas): self.active_msa == self.msas[-1]
        else: self.active_msa = targetmsa
