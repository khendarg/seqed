class ListDict(object):
    def __init__(self, srcdict=None):
        self._dict = dict(srcdict) if srcdict else dict()
        self._list = [self._dict[_] for _ in self._dict]

    def clear(self):
        self._dict = dict()
        for i in reversed(range(len(self._list))): del self._list[i]
        self._list = list()

    def copy(self): return ListDict(self._dict)

    def fromkeys(self, iterable, value=None):
        outdict = self._dict.fromkeys(iterable, value=value)
        return ListDict(outdict)

    def get(self, key, default=None):
        if key in self._dict: return self._dict[key]
        elif key in range(-len(self)+1, len(self)): return self._list[key]
        else: return default

    def items(self): return self._dict.items()

    def keys(self): return self._dict.keys()

    def popitem(self):
        if len(self) == 0: raise KeyError
        else: 
            k = list(self)[-1]
            v = self.pop()
            return (k, v)

    def setdefault(self, key, default=None):
        if key not in self._dict: 
            self._dict[key] = default
            self._list.append(default)
        return self.__getitem__(key)

    def values(self):
        return self._dict.values()

    def __iter__(self): return self._dict.__iter__()

    def _resolve(self, index):
        if type(index) in (int, slice): 
            return self._list
        else: 
            try: 
                int(index)
                return self._list
            except ValueError: return self._dict

    def __len__(self): return len(self._dict)

    def __getitem__(self, index): 
        return self._resolve(index).__getitem__(index)

    def __setitem__(self, index, value): 
        target = self._resolve(index)
        if target is self._dict:
            if index not in self._dict: self._list.append(value)
            self._resolve(index).__setitem__(index, value)
        elif target is self._list:
            self._list.append(value)
            self._dict["{:012x}".format(id(value))] = value

    def _get_keyindex(self, index):
        first = self._resolve(index)
        if first is self._list:
            indices = index
            keys = list(self._dict).__getitem__(index)
        elif first is self._dict:
            keys = index
            indices = list(self._dict).index(keys)
        else: raise LookupError
        return keys, indices

    def pop(self, index=-1):
        k, i = self._get_keyindex(index)
        self._dict.pop(k)
        return self._list.pop(i)

    def __eq__(self, other):
        return self.items() == other.items()

    def remove(self, target):
        if target is None: self.clear()
        for i, k in enumerate(self._dict):
            if target == k: 
                del self._dict[k]
                del self._list[k]
