import argparse
import shlex
import re
import pathlib
import sys

class BypassCall(Exception): pass

class NoExitArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        kwargs["exit_on_error"] = kwargs.get("exit_on_error", False)
        try: 
            super().__init__(*args, **kwargs)
        except TypeError: #this can happen with argparse 1.1 on Python 3.8
            self.exit_on_error = kwargs.pop("exit_on_error")
            super().__init__(*args, **kwargs)
    def exit(self, status=0, message=None): 
        if message:
            self._print_message(message, sys.stderr)
        if self.exit_on_error:
            _sys.exit(status)
        else:
            raise BypassCall
        #if message:
        #    self._print_message(message, _sys.stderr)

def split(rawcommands, include_comment=False):
    for line in re.split("[\n;]+", rawcommands):
        if line.strip(): 
            if include_comment: yield shlex.split(line)
            else:
                out = []
                for _ in shlex.split(line):
                    if (not include_comment) and _.startswith("#"): break
                    else: out.append(_)
                if out: yield out

def lineno_split(rawcommands):
    lineno = 1
    out = []
    for superlineno, superline in enumerate(re.split("\n", rawcommands)):
        for sublineno, line in enumerate(superline.split(";")):
            if line.strip():
                if line.strip().startswith("#"): 
                    out.append((superlineno+1, sublineno+1, "comment", line, line))
                elif "#" in line:
                    out.append((superlineno+1, sublineno+1, "relevant", shlex.split(line[:line.find("#")]), line))
                    out.append((superlineno+1, sublineno+1, "comment", line[line.find("#"):], line))
                else:
                    out.append((superlineno+1, sublineno+1, "relevant", shlex.split(line), line))
            else:
                out.append((superlineno+1, sublineno+1, "empty", line, line))
    return out

def autoparse(value):
    try: return int(value)
    except ValueError:
        try: return float(value)
        except ValueError: 
            try: return autoparse_slice(value)
            except ValueError: return value

def autoparse_slice(value):
    if 1 <= value.count(":") <= 2:
        out = slice(*[(int(x) if x else None) for x in value.split(":")])
        if out.step is None: return slice(out.start, out.stop, 1)
        else: return out
        
    else: return value
        

class ParserContainer(object):
    def __init__(self, workspace, verbosity=0):
        self.workspace = workspace
        self.verbosity = verbosity
        self.initialize()
    def parse_multiline(self, multiline):
        out = []
        for line in split(multiline):
            result = self.parse_line(line)
            if result is not None: out.append(result)
        return out
    def parse_line(self, line):
        return self.generalparser.parse_args(line)
    def initialize(self):
        self.generalparser = NoExitArgumentParser(exit_on_error=False)

        self.subparsers = self.generalparser.add_subparsers(help="Seqed commands")
        #self.parser_XXX = self.subparsers.add_parser("XXX", help="")
        #self.parser_XXX.set_defaults(func=XXX)

        #insert
        self.parser_insert = self.subparsers.add_parser("insert", 
                help="Insert a sequence into an MSA")
        self.parser_insert.set_defaults(func=self.workspace.insert,
                message="Inserted sequence '{self.subseq}' into msa:{self.targetmsa} at position {self.position}")
        self.parser_insert.add_argument("subseq", 
                help="Sequence to insert")
        self.parser_insert.add_argument("position", 
                nargs="?", type=int, default=0, 
                help="Position before which subseq should be inserted")
        self.parser_insert.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to insert subseq into")
        self.parser_insert.add_argument("--targetseq",
                type=autoparse,
                help="Sequence(s) to insert subseq instead of gaps into")

        #append
        self.parser_append = self.subparsers.add_parser("append", 
                help="Append sequences to the end of an MSA")
        self.parser_append.set_defaults(func=self.workspace.append,
                message="Appended sequence '{self.subseq}' into seq:{self.targetseq} in msa:{self.targetmsa}")
        self.parser_append.add_argument("subseq", 
                help="Sequence to append")
        self.parser_append.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to append subseq into")
        self.parser_append.add_argument("--targetseq",
                type=autoparse,
                help="Sequence(s) to append subseq instead of gaps into")
        #extend
        self.parser_extend = self.subparsers.add_parser("extend", 
                help="Append sequences to the end of an MSA")
        self.parser_extend.set_defaults(func=self.workspace.extend,
                message="Appended sequence '{self.subseq}' into seq:{self.targetseq} in msa:{self.targetmsa}")
        self.parser_extend.add_argument("subseq", 
                help="Sequence to extend")
        self.parser_extend.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to extend subseq into")
        self.parser_extend.add_argument("--targetseq",
                type=autoparse,
                help="Sequence(s) to extend subseq instead of gaps into")

        #delete_column
        self.parser_delete_column = self.subparsers.add_parser("delete_column", 
                help="Remove a column from an MSA")
        self.parser_delete_column.set_defaults(func=self.workspace.delete_column,
                message="Deleted column(s) {self.index} from msa:{self.targetmsa}")
        self.parser_delete_column.add_argument("index", 
                type=autoparse, nargs="+",
                help="Columns to remove")
        self.parser_delete_column.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to delete_column from")

        #delete
        self.parser_delete = self.subparsers.add_parser("delete", 
                help="Replace residues in a column from an MSA with gaps")
        self.parser_delete.set_defaults(func=self.workspace.delete,
                message="Replaced residues at {self.index} for seq:{self.targetseq} with gaps")
        self.parser_delete.add_argument("index", 
                type=autoparse, nargs="+",
                help="Columns to remove")
        self.parser_delete.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to delete from")
        self.parser_delete.add_argument("--targetseq",
                type=autoparse,
                help="Sequence(s) whose residues should be turned into gaps")

        #load
        self.parser_load = self.subparsers.add_parser("load", help="Load an MSA into memory.")
        self.parser_load.set_defaults(func=self.workspace.load,
                message="Loaded {self.format}-formatted MSA {self.handle} into memory as msa:{self.name}")
        self.parser_load.add_argument("handle",
                type=pathlib.Path,
                help="Whence to load the MSAs")
        self.parser_load.add_argument("format",
                nargs="?",
                help="MSA format. Limited autodetection is implemented for common FASTA and CLUSTAL extensions.")
        self.parser_load.add_argument("name",
                nargs="?",
                help="Internal name(s) for MSAs")

        #unload
        self.parser_unload = self.subparsers.add_parser("unload", help="Unload an MSA from memory. Useful for bulk processing tasks.")
        self.parser_unload.set_defaults(func=self.workspace.unload,
                message="Unloaded msa:{self.targetmsa} from memory")
        self.parser_unload.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to unload")

        #save
        self.parser_save = self.subparsers.add_parser("save", help="Save an MSA to disk")
        self.parser_save.set_defaults(func=self.workspace.save,
                message="Saved msa:{self.targetmsa} as {self.handle} in {self.format} format")
        self.parser_save.add_argument("handle",
                type=pathlib.Path,
                help="Whither to save the MSAs. The special names 'stdout' and '-' are parsed as /dev/stdout, and 'stderr' is parsed as /dev/stderr.")
        self.parser_save.add_argument("--targetmsa",
                nargs="+", type=autoparse,
                help="Which MSAs to save as indices or MSA names")
        self.parser_save.add_argument("-f", "--format",
                help="MSA format. All formats implemented in Biopython are available, but this typically defaults to 'fasta'")

        #create_seq
        self.parser_create_seq = self.subparsers.add_parser("create_seq", help="Create a new sequence")
        self.parser_create_seq.set_defaults(func=self.workspace.create_seq,
                message="Created a new sequence '{self.seq}' named {self.name} in msa:{self.targetmsa}")
        self.parser_create_seq.add_argument("--targetmsa",
                help="MSA to create sequence in. Defaults to creating a new MSA in an empty workspace.")
        self.parser_create_seq.add_argument("--name",
                help="Name of sequence. Defaults to generating a unique name.")
        self.parser_create_seq.add_argument("--seq",
                default="",
                help="Contents of sequence. Defaults to empty.")

        #set_active
        self.parser_set_active = self.subparsers.add_parser("set_active", help="")
        self.parser_set_active.set_defaults(func=self.workspace.set_active,
                message="Set msa:{self.targetmsa} as active MSA")
        self.parser_set_active.add_argument("--targetmsa",
                type=autoparse,
                help="MSA to make active")

        #exit
        self.parser_set_active = self.subparsers.add_parser("exit", help="Exits Seqed")
        self.parser_set_active.set_defaults(func=self.exit)

        #help
        self.parser_help = self.subparsers.add_parser("help", help="")
        self.parser_help.set_defaults(func=self.generalparser.print_help,
                message="Printed help message")
        self.parser_help = self.subparsers.add_parser("commands", help="")
        self.parser_help.set_defaults(func=self.generalparser.print_help,
                message="Printed help message")


    def exit(self):
        exit(0)

    def call(self, namespace):
        args = namespace._get_args()
        kwargs = dict(namespace._get_kwargs())
        func = kwargs.pop("func")

        if "message" in kwargs: kwargs.pop("message")

        func(*args, **kwargs)

        if self.verbosity > 0:
            if "message" in dir(namespace):
                print("[INFO]", namespace.message.format(self=namespace).replace("None", "default"), file=sys.stderr)

