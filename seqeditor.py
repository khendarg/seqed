#!/usr/bin/env python

import argparse
import pathlib
import seqed
import sys
import shlex
import re

def error(*things, fatal=False):
    print("\033[31m[ERROR]\033[0m", *things, file=sys.stderr)
    if fatal: sys.exit(1)

class SeqeditorParserContainer(seqed.argparser.ParserContainer):
    cwd = pathlib.Path(".")
    def initialize(self):
        super().initialize()
        self.parser_list = self.subparsers.add_parser("list", 
                help="List contents of workspace")
        self.parser_list.set_defaults(func=self.list_workspace,
                message="Listed contents of workspace")
        self.parser_list.add_argument("-r", "--recursive",
                action="store_true",
                help="Recursive list")

        self.parser_ls = self.subparsers.add_parser("ls", 
                help="List contents of working directory.")
        self.parser_ls.set_defaults(func=self.list_directory,
                message="Listed contents of directory")
        self.parser_ls.add_argument("--color",
                default="always",
                help="Color output (always/never)")
        self.parser_ls.add_argument("pathlist",
                nargs="*",
                type=pathlib.Path,
                default=[self.cwd])

        #find
        self.parser_find = self.subparsers.add_parser("find",
                help="Find columns/sequences matching a serach pattern in an MSA")
        self.parser_find.set_defaults(func=self.find,
                message="Searched for /{self.pattern}/")
        self.parser_find.add_argument("pattern",
                type=str)
        self.parser_find.add_argument("--outfmt",
                default="each",
                help="Output format. Supported: each, collapsed, only, only_headered")
        self.parser_find.add_argument("--targetmsa",
                type=seqed.argparser.autoparse,
                help="MSA to search")
        self.parser_find.add_argument("--targetseq",
                type=seqed.argparser.autoparse,
                help="Sequence(s) whose residues should be searched")

    def list_directory(self, pathlist, color="always"):
        filelist = {
                "block":[],
                "char":[],
                "dir":[],
                "fifo":[],
                "file":[],
                "mount":[],
                "reserved":[],
                "socket":[],
                "symlink":[],
                }
        colors = {
                "block":"\033[33m",
                "char":"\033[33m",
                "dir":"\033[34m\033[1m",
                "fifo":"\033[31m",
                "file":"\033[0m",
                "mount":"\033[31m\033[1m",
                "reserved":"\033[31m",
                "socket":"\033[31m",
                "symlink":"\033[36m\033[1m",
                }
        #FIXME: implement glob on FNF or the other way around
        #finalpathlist = []
        #for path in pathlist:
        #    path = path.expanduser()
        #    if path.exists():
        #        finalpathlist.append(path)
        #    else:
        #        finalpathlist.extend(pathlib.Path(".").glob(path))
        #print(finalpathlist)
        #for path in finalpathlist:
        for path in pathlist:
            for fn in sorted(path.glob("*")):
                if fn.is_block_device(): filelist["block"].append(fn)
                elif fn.is_char_device(): filelist["char"].append(fn)
                elif fn.is_dir(): filelist["dir"].append(fn)
                elif fn.is_fifo(): filelist["fifo"].append(fn)
                elif fn.is_file(): filelist["file"].append(fn)
                elif fn.is_mount(): filelist["mount"].append(fn)
                elif fn.is_reserved(): filelist["reserved"].append(fn)
                elif fn.is_socket(): filelist["socket"].append(fn)
                elif fn.is_symlink(): filelist["symlink"].append(fn)

            entries = []
            for category in filelist:
                for fn in filelist[category]:
                    entry = ""
                    if color == "always":
                        if category == "file" and ((fn.stat().st_mode % 2) or ((fn.stat().st_mode >> 3) % 2) or ((fn.stat().st_mode >> 6) % 2)):
                            print("{:o}".format(fn.stat().st_mode))
                            entry += "\033[32m\033[1m"
                        else:
                            entry += colors[category]
                    entry += str(fn.name)
                    if color == "always":
                        entry += "\033[0m"
                    entries.append(entry)
            print("\n".join(entries))
                


    def list_workspace(self, recursive=False):
        shape = self.workspace.get_shape()
        print("#index\tname\tsequences\tlength")
        for msaindex, (msaname, height, width) in enumerate(zip(self.workspace.msas, shape[1], shape[2])):
            print("{}\t{}\t{}\t{}".format(msaindex, msaname, height, width))
            if recursive: 
                for recordindex, record in enumerate(self.workspace.msas[msaindex]):
                    print("{}/{}\t{}\t1\t{}".format(
                        msaindex, recordindex, 
                        record.id, 
                        len(record.seq.replace("-", ""))
                        ))
        print("{} MSAs loaded".format(shape[0]))

    def find(self, pattern, targetmsa=None, targetseq=None, outfmt="each"):
        """Find matches in an MSA

        Note that this method as currently implemented does not return overlapping hits. This may be a concern for low-complexity sequences with extensive repeats.

        Parameters
        ----------
        pattern : str
            Pattern to search for. Accepts Python regular expressions.
        targetmsa : NoneType or int or MultipleSeqAlignment 
            See _operate_on_sequences for details.
        targetseq : NoneType or int or list or range or slice or str
            See _operate_on_sequences for details.

        Returns
        -------
        allhits : dict
            A dictionary containing hits binned by sequence ID
        """

        targetmsa = self.workspace.active_msa if targetmsa is None else targetmsa
        hitseq = []
        self.workspace._operate_on_sequences(targetmsa=targetmsa, targetseq=targetseq,
                hit=lambda record: hitseq.append(record),
                miss=lambda record: None,
                )
        allhits = {}
        hitseqs = {}
        for record in hitseq:
            recordhits = list(re.finditer(pattern, str(record.seq)))
            if recordhits: 
                if record.id not in allhits:
                    allhits[record.id] = []
                allhits[record.id].extend(recordhits)
                hitseqs[record.id] = record

        if outfmt == "each":
            for recordid in allhits:
                for match in allhits[recordid]:
                    print(f"{recordid}\t{match.span()[0]}\t{match.span()[1]}")
        elif outfmt == "collapsed":
            hits = {}
            for recordid in allhits:
                for match in allhits[recordid]:
                    if (match.span()[0],match.span()[1]) not in hits: hits[match.span()[0],match.span()[1]] = 0
                    hits[match.span()[0],match.span()[1]] += 1
            sortme = [(k, hits[k]) for k in hits]
            sortme.sort(key=lambda x: (-x[1], x[0]))
            for k, v in sortme:
                print(f"{v}\t{k[0]}\t{k[1]}")
        elif outfmt == "only":
            for recordid in allhits:
                for match in allhits[recordid]:
                    print(hitseqs[recordid].seq[match.span()[0]:match.span()[1]])
        elif outfmt == "only_headered":
            for recordid in allhits:
                for match in allhits[recordid]:
                    print(f">{recordid} ({match.span()[0]}:{match.span()[1]})")
                    print(hitseqs[recordid].seq[match.span()[0]:match.span()[1]])

        return allhits

def main(args):
    workspace = seqed.seqed.SeqedModel()
    parser = SeqeditorParserContainer(workspace, verbosity=args.v)
    if args.infile:
        if args.format is None: fmt = [None] * len(args.infile)
        elif len(args.format) == 1: fmt = [args.format] * len(args.infile)
        elif len(args.format) == len(args.infile): fmt = args.format
        else: error("Format list must contain 0, 1, or len(infiles) formats")
        for handle, format in zip(args.infile, fmt):
            workspace.load(handle, format=format, name=handle.stem)
            if args.v: print("[INFO]", "Loaded MSAs from {}".format(handle), file=sys.stderr)

    if args.c or args.r: 
        if args.c: rawcommands = args.c
        else:
            with open(args.r) as fh: rawcommands = fh.read()

        for lineno, sublineno, linetype, content, rawline in seqed.argparser.lineno_split(rawcommands):
            call = True
            if linetype != "relevant": continue
            try: result = parser.parse_line(content)
            except argparse.ArgumentError:
                error("Could not parse line {lineno}.{sublineno}: {rawline}".format(
                    lineno=lineno, 
                    sublineno=sublineno,
                    rawline=repr(rawline),)
                    )
                call = False
            except seqed.argparser.BypassCall: call = False
            if call:
                try: parser.call(result)
                except Exception as e: error("{}: {}".format(type(e).__name__, e))
    else: 
        print("seqed {}".format(seqed.__version__), file=sys.stderr)
        print("Type \"help\" or \"commands\" for more information", file=sys.stderr)
        run = True
        lineno = 1
        while run:
            results = []
            call = True
            try: 
                multiline = seqed.argparser.lineno_split(input("^^^ "))
                for lineno, sublineno, linetype, line, rawline in multiline:
                    if linetype == "relevant": results.append(parser.parse_line(line))
            except EOFError: run = False
            except KeyboardInterrupt: 
                print()
                call = False
            except argparse.ArgumentError:
                error("Could not parse line {lineno}: {rawline}".format(
                    lineno=lineno, 
                    rawline=repr(rawline),)
                    )
                call = False
            except seqed.argparser.BypassCall: call = False
            if call:
                try:
                    for result in results: parser.call(result)
                except Exception as e: error("{}: {}".format(type(e).__name__, e))
            lineno += 1
        print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", 
            type=pathlib.Path, nargs="*", 
            help="Sequence files to read")
    parser.add_argument("--format", 
            nargs="+", 
            help="Single format for all sequence files or individual formats for each sequence file")
    srcgroup = parser.add_argument_group("command source arguments (mutually exclusive)")
    srcparser = srcgroup.add_mutually_exclusive_group()
    srcparser.add_argument("-c", 
            help="Commands to run")
    srcparser.add_argument("-r", 
            type=pathlib.Path, 
            help="Seqed script to run")
    #parser.add_argument("-q",
    #        action="store_true",
    #        help="Quiet mode")
    parser.add_argument("-v",
            action="store_true",
            help="Verbose mode")
    args = parser.parse_args()

    main(args)
