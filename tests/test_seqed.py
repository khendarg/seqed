#!/usr/bin/env python

import unittest
import seqed
import io
import os

class TestListDict(unittest.TestCase):
    x = seqed.types.ListDict({"abc":123, "def":456, "ghi":789})
    def test_listdict_clear(self):
        x = seqed.types.ListDict(self.x)
        assert len(x) == 3
        x.clear()
        assert len(x) == 0

    def test_listdict_copy(self):
        x = seqed.types.ListDict(self.x)
        y = x.copy()
        assert x is not y
        assert x == y

    def test_listdict_get(self):
        x = seqed.types.ListDict(self.x)
        assert x.get("def") == 456
        assert x.get("xyz") is None

    def test_listdict_popitem(self):
        x = seqed.types.ListDict(self.x)
        assert x.popitem() == ("ghi", 789)
        assert len(x) == 2

    def test_listdict_setdefault(self):
        x = seqed.types.ListDict(self.x)
        assert len(x) == 3
        x.setdefault("def", 321)
        assert len(x) == 3
        assert x == self.x
        assert x is not self.x

        x.setdefault("jkl", 12)
        assert len(x) == 4
        assert x != self.x
        assert x["jkl"] == 12
        assert x[3] == 12

    def test_listdict_dict(self):
        x = dict(self.x)
        assert len(x) == 3

    def test_listdict_getitem(self):
        x = seqed.types.ListDict(self.x)
        assert x[1] == 456
        assert x["def"] == 456
        assert x["def"] == x[1]
    def test_listdict_resolve(self):
        x = seqed.types.ListDict(self.x)
        assert type(x._resolve(0)) is list
        assert type(x._resolve("abc")) is dict
        assert type(x._resolve(slice(0,2,1))) is list

    def test_listdict_pop(self):
        x = seqed.types.ListDict(self.x)
        assert len(x) == 3
        assert x.pop() == 789
        assert len(x) == 2
        assert x.pop("abc") == 123
        assert len(x) == 1

class TestModel(unittest.TestCase):
    def test_init(self):
        w = seqed.seqed.SeqedModel()
        assert len(w) == 0

    def test_create(self):
        w = seqed.seqed.SeqedModel()
        assert w.active_seqrecord is None
        assert w.active_msa is None
        assert w.get_shape() == (0, [], [])
        w.create_seq()
        assert w.get_shape() == (1, [1], [0])
        assert w.active_seqrecord is not None
        assert w.active_msa is not None
        assert w.active_seqrecord is w.active_msa[0]
        w.create_seq()
        assert w.get_shape() == (1, [2], [0])

    def test_extend(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq()
        w.extend("TEST")
        assert w.get_shape() == (1, [1], [4])
        w.create_seq()
        assert w.get_shape() == (1, [2], [4])
        w.extend("MORE")
        assert w.get_shape() == (1, [2], [8])
        w.extend("DATA", targetseq=1)
        assert w.get_shape() == (1, [2], [12])
        assert len(str(w.active_msa[0].seq).replace("-", "")) == 8
        assert len(str(w.active_msa[1].seq).replace("-", "")) == 8

    def test_insert(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq()
        w.insert("TEST")
        assert w.get_shape() == (1, [1], [4])
        assert w.active_msa[0].seq == "TEST"
        w.insert("INSERT", 2)
        assert w.active_msa[0].seq == "TEINSERTST"
        w.create_seq()
        w.insert("ALLSEQ", 5)
        assert w.active_msa[0].seq == "TEINSALLSEQERTST"
        assert w.active_msa[1].seq == "-----ALLSEQ-----"

    def test_delete_column(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq()
        w.create_seq()
        w.create_seq()
        w.extend("TESTSEQUENCE")
        assert w.get_shape() == (1, [3], [12])

        w.delete_column(slice(1, -1, 2))
        assert w.get_shape() == (1, [3], [7])
        assert w.active_msa[0].seq == "TSSQECE"

        w.delete_column(1)
        assert w.get_shape() == (1, [3], [6])
        assert w.active_msa[0].seq == "TSQECE"

    def test_delete_to_gaps(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq()
        w.create_seq()
        w.create_seq()
        w.extend("TESTSEQUENCE")
        assert w.get_shape() == (1, [3], [12])
        w.delete(slice(1, -1, 2), targetseq=1)
        assert w.get_shape() == (1, [3], [12])
        assert w.active_msa[0].seq == "TESTSEQUENCE"
        assert w.active_msa[1].seq == "T-S-S-Q-E-CE"
        assert w.active_msa[0].seq == w.active_msa[2].seq
        
    def test_seq_id_match(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq(name="first")
        w.create_seq()
        w.create_seq(name="second")
        w.create_seq(name="first")
        w.create_seq(name="FIRST")

        assert w.get_shape() == (1, [5], [0])

        w.extend("FIRSTSEQ", targetseq="/first|FIRST/")
        assert w.get_shape() == (1, [5], [8])
        assert w.active_msa[0].seq == "FIRSTSEQ"
        assert w.active_msa[3].seq == "FIRSTSEQ"

        w.delete(range(5), targetseq=r"first")
        assert w.get_shape() == (1, [5], [8])
        assert w.active_msa[0].seq == "-----SEQ"
        assert w.active_msa[3].seq == "-----SEQ"
        assert w.active_msa[4].seq == "FIRSTSEQ"

    def test_save(self):
        w = seqed.seqed.SeqedModel()
        w.create_seq()
        w.extend("FIRSTSEQ")
        with io.StringIO() as outbuffer:
            w.save(outbuffer)
            outbuffer.flush()
            outbuffer.seek(0)
            contents = outbuffer.read()
            assert contents.startswith(">")
        with io.StringIO() as outbuffer:
            w.save(outbuffer, format="clustal")
            outbuffer.flush()
            outbuffer.seek(0)
            contents = outbuffer.read()
            assert contents.startswith("CLUSTAL")

    def test_load(self):
        w = seqed.seqed.SeqedModel()
        with io.StringIO() as inbuffer:
            inbuffer.write(">test\nTESTSEQUENCE\n>test2\nMORESEQUENCE")
            inbuffer.flush()
            inbuffer.seek(0)
            w.load(inbuffer)
            assert w.get_shape() == (1, [2], [12])
        with io.StringIO() as inbuffer:
            inbuffer.write("""CLUSTAL
test                                TESTSEQUENCE
test2                               MORESEQUENCE

CLUSTAL
test3                               TEST
test4                               MORE""")
            inbuffer.flush()
            inbuffer.seek(0)
            w.load(inbuffer)
            assert w.get_shape() == (3, [2, 2, 2], [12, 12, 4])

class TestArgparser(unittest.TestCase):
    def test_split(self):
        assert len(list(seqed.argparser.split("hello\nworld", include_comment=False))) == 2
        assert len(list(seqed.argparser.split("hello\nworld", include_comment=True))) == 2

        assert len(list(seqed.argparser.split("hello;\nworld", include_comment=False))) == 2
        assert len(list(seqed.argparser.split("hello;\nworld", include_comment=True))) == 2

        assert len(list(seqed.argparser.split("hello\n;\nworld", include_comment=False))) == 2
        assert len(list(seqed.argparser.split("hello\n;\nworld", include_comment=True))) == 2

        assert len(list(seqed.argparser.split("hello\n#new;world", include_comment=False))) == 2
        assert len(list(seqed.argparser.split("hello\n#new;world", include_comment=True))) == 3

    def shh_test_parser_help(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        with self.assertRaises(SystemExit): parser.parse_line(["-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["insert", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["append", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["delete_columns", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["delete", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["load", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["unload", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["save", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["create_seq", "-h"])
        with self.assertRaises(SystemExit): parser.parse_line(["set_active", "-h"])
    def test_parser_create_seq(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        assert w.get_shape() == (0, [], [])
        [parser.call(result) for result in parser.parse_multiline("create_seq")]
        assert w.get_shape() == (1, [1], [0])

    def test_parser_append(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        assert w.get_shape() == (0, [], [])
        [parser.call(result) for result in parser.parse_multiline("create_seq")]
        assert w.get_shape() == (1, [1], [0])
        [parser.call(result) for result in parser.parse_multiline("create_seq")]
        assert w.get_shape() == (1, [2], [0])
        [parser.call(result) for result in parser.parse_multiline("append TEST")]
        assert w.get_shape() == (1, [2], [4])
        [parser.call(result) for result in parser.parse_multiline("append SEQ --targetseq 0")]
        assert w.get_shape() == (1, [2], [7])
        assert w.active_msa[0].seq == "TESTSEQ"
        assert w.active_msa[1].seq == "TEST---"

    def test_parser_insert(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("create_seq;insert TEST")]
        assert w.get_shape() == (1, [1], [4])
        assert w.active_msa[0].seq == "TEST"
        [parser.call(result) for result in parser.parse_multiline("insert INSERT 2")]
        assert w.active_msa[0].seq == "TEINSERTST"
        [parser.call(result) for result in parser.parse_multiline("create_seq;insert ALLSEQ 5")]
        assert w.active_msa[0].seq == "TEINSALLSEQERTST"
        assert w.active_msa[1].seq == "-----ALLSEQ-----"

    def test_parser_delete_column(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("create_seq;create_seq;create_seq;append TESTSEQUENCE")]
        assert w.get_shape() == (1, [3], [12])
        [parser.call(result) for result in parser.parse_multiline("delete_column 1:-1:2")]
        assert w.get_shape() == (1, [3], [7])
        assert w.active_msa[0].seq == "TSSQECE"
        [parser.call(result) for result in parser.parse_multiline("delete_column 1")]
        assert w.active_msa[0].seq == "TSQECE"


    def test_parser_delete_to_gaps(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("create_seq;create_seq;create_seq;append TESTSEQUENCE")]
        assert w.get_shape() == (1, [3], [12])
        [parser.call(result) for result in parser.parse_multiline("delete 1:-1:2 --targetseq 1")]
        assert w.get_shape() == (1, [3], [12])
        assert w.active_msa[0].seq == "TESTSEQUENCE"
        assert w.active_msa[1].seq == "T-S-S-Q-E-CE"
        assert w.active_msa[0].seq == w.active_msa[2].seq
        
    def test_parser_seq_id_match(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("create_seq --name first;create_seq;create_seq --name second; create_seq --name first; create_seq --name FIRST")]
        assert w.get_shape() == (1, [5], [0])
        [parser.call(result) for result in parser.parse_multiline("append FIRSTSEQ --targetseq /first|FIRST/")]

        assert w.get_shape() == (1, [5], [8])
        assert w.active_msa[0].seq == "FIRSTSEQ"
        assert w.active_msa[3].seq == "FIRSTSEQ"

        [parser.call(result) for result in parser.parse_multiline("delete 0:5 --targetseq first")]
        assert w.get_shape() == (1, [5], [8])
        assert w.active_msa[0].seq == "-----SEQ"
        assert w.active_msa[3].seq == "-----SEQ"
        assert w.active_msa[4].seq == "FIRSTSEQ"

    def test_parser_load(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("load testdata/ferroportin.aln")]
        assert w.get_shape() == (1, [9], [750])

    def test_parser_save(self):
        w = seqed.seqed.SeqedModel()
        parser = seqed.argparser.ParserContainer(w)
        [parser.call(result) for result in parser.parse_multiline("load testdata/ferroportin.aln")]
        assert w.get_shape() == (1, [9], [750])
        try:
            [parser.call(result) for result in parser.parse_multiline("save testdata/tempaln.faa")]
            [parser.call(result) for result in parser.parse_multiline("load testdata/tempaln.faa")]
            assert w.get_shape() == (2, [9, 9], [750, 750])
        finally: 
            try: os.remove("testdata/tmpaln.faa")
            except FileNotFoundError: pass

        try:
            [parser.call(result) for result in parser.parse_multiline("save testdata/tempaln.aln --targetmsa 1")]
            [parser.call(result) for result in parser.parse_multiline("load testdata/tempaln.aln")]
            assert w.get_shape() == (3, [9, 9, 9], [750, 750, 750])
        finally: 
            try: os.remove("testdata/tmpaln.aln")
            except FileNotFoundError: pass
        
if __name__ == "__main__":
    unittest.main()
